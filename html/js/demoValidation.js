$(document).ready(function() {

	$("#demoForm").validate({

		onfocusout: false,
		onkeyup: false,
		onclick: false,
		rules: {
			"password": {
				required: true,
				maxlength: 8
			},
			"passwordnew": {
				required: true,
				minlength: 8
			},
			"password_conflim": {
				required: true,
				minlength: 8
			},
		},
		messages: {
			"password": {
				required: "Bắt buộc nhập password",
				maxlength: "Hãy nhập tối đa 8 ký tự"
			},
			"passwordnew": {
				required: "Bắt buộc nhập passwordnew",
				minlength: "Hãy nhập ít nhất 8 ký tự"
			},
			"password_conflim": {
				required: "Bắt buộc nhập password_conflim",
				minlength: "Hãy nhập ít nhất 8 ký tự"
			},
		},
		submitHandler: function(form) {
			console.log('submit nè');
	      	form.submit();
	    }
	});
});